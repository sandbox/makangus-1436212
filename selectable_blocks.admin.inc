<?php
/**
 * @file
 * Administration page callbacks for the Selectable Blocks module
 */

/**
 * Form builder. Configure Selectable Blocks.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function selectable_blocks_admin_settings() {
  $types = node_type_get_types();
  foreach ($types as $node_type) {
    $node_type_options[$node_type->type] = $node_type->name;
  }

  $form['selectable_blocks_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allow Selectable Blocks on these content types'),
    '#options' => $node_type_options,
    '#default_value' => variable_get('selectable_blocks_node_types', array()),
    '#description' => t('The Selectable Blocks tab will be shown on the selected content types'),
  );


  $default_theme = variable_get('theme_default', 'bartik');
  $regions = system_region_list($default_theme);


  $form['selectable_blocks_regions'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Allow Selectable Blocks on these regions'),
    '#options' => $regions,
    '#default_value' => variable_get('selectable_blocks_regions', array()),
    '#description' => t('Select the regions to be configured as Selectable Region'),
  );


  $form['#submit'][] = 'selectable_blocks_admin_settings_submit';
  return system_settings_form($form);
}
